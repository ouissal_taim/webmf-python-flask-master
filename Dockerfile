FROM python:3.8

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY requirements.txt /usr/src/app/
RUN pip install -r requirements.txt

RUN useradd appuser && chown -R appuser /app
USER appuser

# Adds permission to appuser (non-root) for access to the /extra folder
RUN chown -R appuser /extra

# Bundle app source
COPY . /usr/src/app

EXPOSE 5000
ENTRYPOINT ["python"]
CMD ["app.py"]

