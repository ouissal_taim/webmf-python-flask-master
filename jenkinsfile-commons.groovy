/**
 * This file provides common stuff to be used in the pipelines.
 * It is important to load it after repo checkout is done: see https://github.com/jenkinsci/pipeline-plugin/blob/master/TUTORIAL.md#triggering-manual-loading
 *
 */

/**
 * Dumps some info about the environment.
 * @return
 */
 

def dumpEnv() {


    echo "Path is ${env.PATH}"
    echo "Current dir is ${pwd()}"


}

def success() {
    currentBuild.result = "SUCCESS"
}

def handleError(err) {
    echo "Caught: ${err}"
    currentBuild.result = "FAILURE"
    throw any //rethrow exception to prevent the build from proceeding
}

/**
 * Sends notifications if necessary.
 * @return
 */
def sendNotifications() {
    step([$class: "Mailer", notifyEveryUnstableBuild: true, recipients: emailextrecipients([[$class: "CulpritsRecipientProvider"], [$class: "RequesterRecipientProvider"]])])
}

def dockerCleanup(){
    //  do || true so that Jenkins won't notice if there is an error in any of those commands
    //
    // delete images w/o tags
    sh 'docker rmi $(docker images | grep "^<none>" | awk \'{print $3}\')      || true'
    // remove dangling images
    sh 'docker images -q -f dangling=true | xargs --no-run-if-empty docker rmi || true'
    // remove dangling volumes
    sh 'docker volume ls -qf dangling=true | xargs -r docker volume rm         || true'
    // remove all images! the ones that are running won't be removed.
    // others are pushed to registry anyway
    sh 'docker images -q  | xargs --no-run-if-empty docker rmi                 || true'
}

def prompt(msg) {
    try{
        timeout(time: 120, unit: 'SECONDS') {
            try {
                input msg
                echo "User gave approval"
                return true
            } catch (ignored) {
                echo "User pressed NO"
                return false
            }
        }
    } catch (ignored){
        echo "Nothing is pressed and prompt timed out"
        return false
    }
}

return this;
