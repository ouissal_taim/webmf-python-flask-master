#!/usr/bin/env groovy

def deployP = "playbooks/staging-redeploy.yml"


node {
      
    env.PATH = "C:/Users/dell/AppData/Local/Programs/Python/Python37/"
     
    def commons

    stage("log") {
        checkout scm
        commons = load("jenkinsfile-commons.groovy")

        commons.dumpEnv()
    }

    try {

        stage("build-deps") {
                sh "pip install -r requirements.txt"
        }

        stage("test") {
            dir("project-bots") {
                sh "test.py"
            }
        }

        stage("deploy to staging") {
            def deploy = commons.prompt('Deploy to staging system?')
            if (!deploy) {
                echo "Skipping deployment to staging system"
            } else {
                echo "Gonna deploy now to staging system"

                sh "echo \"${env.VAULT_PASS}\" > /tmp/${env.BUILD_TAG}_vault_pass"

                dir("project-ops") {
                    sh "ansible-playbook -i inventory -f 5 --vault-password-file /tmp/${env.BUILD_TAG}_vault_pass \"${deployPlaybook}\""
                }
            }
        }

        commons.success()

    } catch (err) {
        commons.handleError(err)
    } finally {

        catchError {
            stage("clean up") {
                // delete the temporary vault passfile. don't fail if the file doesn't exist
                sh "rm /tmp/${env.BUILD_TAG}_vault_pass || true"
            }
        }

        commons.sendNotifications()
    }
}
